var time__utils_8h =
[
    [ "NRP_LOG_TIME", "time__utils_8h.html#a1235b51ed357e4bcc6617c2ad8efc8b0", null ],
    [ "NRP_LOG_TIME_BLOCK", "time__utils_8h.html#ab33b74f2735ed14b63bf2dbae0a00d12", null ],
    [ "NRP_LOG_TIME_BLOCK_WITH_COMMENT", "time__utils_8h.html#a56f4db298b9d35d0b69d400cb13e8e80", null ],
    [ "NRP_LOG_TIME_SET_START", "time__utils_8h.html#a51eca6a3e7df61fe25ead58779917b97", null ],
    [ "NRP_LOG_TIME_WITH_COMMENT", "time__utils_8h.html#ae41c5ace7ae7f306bf8c3d0bdf2bf39c", null ],
    [ "SimulationTime", "time__utils_8h.html#aadff625b29e39dc2d75a7d476d5040c1", null ],
    [ "getRoundedRunTimeMs", "time__utils_8h.html#a9d0c24830e4fec08df3551fe7548a8bc", null ],
    [ "getTimestamp", "time__utils_8h.html#a267ef521bc704496e86a915730403e7b", null ],
    [ "toSimulationTimeFromSeconds", "time__utils_8h.html#aa169593657be4951694eef18fd2b2734", null ]
];