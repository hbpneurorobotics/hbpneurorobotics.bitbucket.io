var class_event_loop_engine =
[
    [ "EventLoopEngine", "class_event_loop_engine.html#aff890ed5d9b0aed89aa66bb03ef979c5", null ],
    [ "~EventLoopEngine", "class_event_loop_engine.html#a21885268cf0e7a2cfa4de95b77afc02d", null ],
    [ "initializeCB", "class_event_loop_engine.html#a22e59908c0650cb65d72f6035e425971", null ],
    [ "runLoopCB", "class_event_loop_engine.html#aacbf2b52fcd818f84995cd763eecdb53", null ],
    [ "shutdownCB", "class_event_loop_engine.html#a5d090984334e3ea20063871ac2c87efe", null ]
];