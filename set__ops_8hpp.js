var set__ops_8hpp =
[
    [ "big_small_intersection_size", "set__ops_8hpp.html#ac1ffb52ab9741e6a3317156732666c7a", null ],
    [ "includes_elm", "set__ops_8hpp.html#ab146b3d0f166f924b1f6387b89997437", null ],
    [ "intersection_size", "set__ops_8hpp.html#a6ce21294f979be5b409561a7c5e7139d", null ],
    [ "operator*", "set__ops_8hpp.html#a622ece470fbceabfb9974ea5c7d8edbc", null ],
    [ "operator+", "set__ops_8hpp.html#abdba448c0bab02b62cec7d4f25aaf7df", null ],
    [ "operator+=", "set__ops_8hpp.html#a7ec61c46c8e5cb59adb7fed733e1c3a8", null ],
    [ "operator-", "set__ops_8hpp.html#a111c59c42c62d18814378652f441b02b", null ],
    [ "operator-=", "set__ops_8hpp.html#af2accb7c4f5465e9c27a8d15ae43ba07", null ],
    [ "operator==", "set__ops_8hpp.html#a82b46bf5aac6530c52994d11699b13a6", null ],
    [ "set_difference_size", "set__ops_8hpp.html#adbee5fc31f0e1426cb70959ad316603f", null ],
    [ "symm_diff", "set__ops_8hpp.html#a8662740216dc497cf997630ef350649c", null ],
    [ "union_size", "set__ops_8hpp.html#a7b9a7022be4769abb342fabcb2338a47", null ]
];