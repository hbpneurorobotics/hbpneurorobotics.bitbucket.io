var event_loop =
[
    [ "Computational Graph", "computational_graph.html", [
      [ "Graph Edges: Ports", "computational_graph.html#graph_ports", null ],
      [ "Computational Nodes", "computational_graph.html#computational_nodes", [
        [ "Computational Node Types", "computational_graph.html#computational_node_types", [
          [ "Input Nodes", "computational_graph.html#input_node", null ],
          [ "Output Nodes", "computational_graph.html#output_node", null ],
          [ "Functional Nodes", "computational_graph.html#functional_node", null ]
        ] ],
        [ "Computational Node Implementations", "computational_graph.html#node_implementations", [
          [ "ROS Nodes", "computational_graph.html#ros_nodes", null ],
          [ "Engine Nodes", "computational_graph.html#engine_nodes", null ],
          [ "SpiNNaker Nodes", "computational_graph.html#spinnaker_nodes", null ],
          [ "MQTT Nodes", "computational_graph.html#mqtt_nodes", null ],
          [ "Time Nodes", "computational_graph.html#time_nodes", null ]
        ] ]
      ] ],
      [ "Execution Order of Nodes in the Graph", "computational_graph.html#graph_layers", null ],
      [ "Computational Graph Execution Modes", "computational_graph.html#graph_exec_modes", null ],
      [ "Data Management in the Computational Graph", "computational_graph.html#graph_data_policies", [
        [ "Data Conversion in Input Ports", "computational_graph.html#graph_data_conversion", null ],
        [ "Data Caching and Coherence", "computational_graph.html#graph_data_caching", null ]
      ] ],
      [ "Node Execution Policies", "computational_graph.html#node_policies", [
        [ "Input Node", "computational_graph.html#input_node_policies", null ],
        [ "Functional Node", "computational_graph.html#functional_node_policies", null ],
        [ "Output Node", "computational_graph.html#output_node_policies", null ]
      ] ],
      [ "Computational Graph Lifecycle", "computational_graph.html#graph_fsm", null ]
    ] ],
    [ "Instantiating a Computational Graph in Python", "python_graph.html", [
      [ "@FunctionalNode", "python_graph.html#functional_node_decorator", null ],
      [ "@FromFunctionalNode", "python_graph.html#from_functional_node_decorator", null ],
      [ "@FromEngine", "python_graph.html#from_engine_decorator", null ],
      [ "@ToEngine", "python_graph.html#to_engine_decorator", null ],
      [ "@RosSubscriber", "python_graph.html#ros_subscriber_decorator", null ],
      [ "@RosPublisher", "python_graph.html#ros_publisher_decorator", null ],
      [ "@FromSpiNNaker", "python_graph.html#from_spinnaker_decorator", null ],
      [ "@ToSpiNNaker", "python_graph.html#to_spinnaker_decorator", null ],
      [ "@MQTTSubscriber", "python_graph.html#mqtt_subscriber_decorator", null ],
      [ "@MQTTPublisher", "python_graph.html#mqtt_publisher_decorator", null ],
      [ "@Clock", "python_graph.html#clock_decorator", null ],
      [ "@Iteration", "python_graph.html#iteration_decorator", null ],
      [ "Putting the Examples Together", "python_graph.html#python_graph_example", null ]
    ] ],
    [ "Event Loop configuration in experiments", "event_loop_configuration.html", [
      [ "Experiment Configuration", "event_loop_configuration.html#event_loop_configuration_parameters", null ],
      [ "Interacting with Engines from the Event Loop", "event_loop_configuration.html#event_loop_engine_interaction", null ],
      [ "Running a Computational Graph Synchronously in an FTILoop", "event_loop_configuration.html#fti_loop_with_computational_graph", null ]
    ] ],
    [ "Using C++ Pre-compiled Functional Nodes in the Computational Graph", "fn_cpp_nodes.html", [
      [ "Valid Function Signature", "fn_cpp_nodes.html#fn_cpp_nodes_valid_signature", null ],
      [ "Instantiating Compiled Functional Nodes in a Computational Graph", "fn_cpp_nodes.html#fn_cpp_nodes_instantiation_cg", [
        [ "Note on the use of \"createFNFromFactoryModule\" Python function", "fn_cpp_nodes.html#fn_cpp_nodes_python_functions", null ]
      ] ],
      [ "Matching Port types when connecting C++ Pre-compiled Functional Nodes", "fn_cpp_nodes.html#fn_cpp_nodes_type_match", [
        [ "ROS Nodes", "fn_cpp_nodes.html#fn_cpp_nodes_type_match_ros", null ],
        [ "MQTT Nodes", "fn_cpp_nodes.html#fn_cpp_nodes_type_match_mqtt", null ],
        [ "Engine Nodes", "fn_cpp_nodes.html#fn_cpp_nodes_type_match_engine", null ]
      ] ],
      [ "Using the build_fn_factory_module.sh script", "fn_cpp_nodes.html#fn_cpp_nodes_script_usage", [
        [ "SOURCE_FILENAME", "fn_cpp_nodes.html#fn_cpp_nodes_script_usage_source", null ],
        [ "NRP_PROTO_MSGS_PACKAGES", "fn_cpp_nodes.html#fn_cpp_nodes_script_usage_proto", null ],
        [ "NRP_ROS_MSGS_PACKAGES", "fn_cpp_nodes.html#fn_cpp_nodes_script_usage_ros", null ]
      ] ],
      [ "Limitations", "fn_cpp_nodes.html#fn_cpp_nodes_limitations", null ]
    ] ],
    [ "Executing Engines Asynchronously and in Real-time with the Event Loop", "async_experiments.html", [
      [ "Asynchronous Engines", "async_experiments.html#async_engines", [
        [ "Executables", "async_experiments.html#async_engines_exec", null ],
        [ "Configuration", "async_experiments.html#async_engines_config", null ],
        [ "Data Exchange through MQTT topics", "async_experiments.html#async_engines_comm", null ]
      ] ],
      [ "Adapting Experiments to Run Asynchronously", "async_experiments.html#async_experiment_guide", [
        [ "Data Exchange and Experiment Code Changes", "async_experiments.html#async_experiment_guide_data_exchange", null ],
        [ "Experiment Configuration", "async_experiments.html#async_experiment_guide_config", null ],
        [ "Launching the Experiment", "async_experiments.html#async_experiment_guide_launching", null ],
        [ "Runtime Experiment Behavior", "async_experiments.html#async_experiment_guide_behavior", null ]
      ] ],
      [ "Example Experiments", "async_experiments.html#async_engines_example_experiments", [
        [ "examples/event_loop_examples/husky_braitenberg_async", "async_experiments.html#async_engines_example_experiments_husky", null ],
        [ "examples/event_loop_examples/tf_exchange_async", "async_experiments.html#async_engines_example_experiments_tf_exchange", null ],
        [ "examples/event_loop_examples/opensim_control_async", "async_experiments.html#async_engines_example_experiments_opensim", null ],
        [ "gRPC Engines generated from template", "async_experiments.html#async_engines_example_experiments_grpc_template", null ]
      ] ]
    ] ]
];