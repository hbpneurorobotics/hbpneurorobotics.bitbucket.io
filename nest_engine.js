var nest_engine =
[
    [ "NEST JSON Engine", "nest_json.html", [
      [ "DataPacks", "nest_json.html#nest_json_datapacks", null ],
      [ "Engine Configuration Parameters", "nest_json.html#nest_json_configuration", null ],
      [ "Schema", "nest_json.html#nest_json_schema", null ]
    ] ],
    [ "NEST Server Engine", "nest_server.html", [
      [ "DataPacks", "nest_server.html#nest_server_datapacks", null ],
      [ "Engine Configuration Parameters", "nest_server.html#nest_server_configuration", null ],
      [ "Schema", "nest_server.html#nest_server_schema", null ]
    ] ]
];