var class_simulation_data_manager =
[
    [ "clear", "class_simulation_data_manager.html#a01f43ac9a4fe00cd5b124b0c02ac4b32", null ],
    [ "clearTrajectory", "class_simulation_data_manager.html#a4530060fcd7542bc3cebd08332b0104f", null ],
    [ "getDoneFlag", "class_simulation_data_manager.html#af855dd185a15f968b249f4092c87f7b5", null ],
    [ "getEngineDataPacks", "class_simulation_data_manager.html#a8a0a82f5c2779ff811eb4cf937f88d49", null ],
    [ "getPreprocessingDataPacks", "class_simulation_data_manager.html#a838d5c76d3613366eb71bfdbb89dc74d", null ],
    [ "getStatusDataPacks", "class_simulation_data_manager.html#ab66af600773960311f4d6273d332469f", null ],
    [ "getTrajectory", "class_simulation_data_manager.html#ae752d70656639e0150d8e994514a0985", null ],
    [ "getTransceiverDataPacks", "class_simulation_data_manager.html#a80e375e9befd8024b3510626722233b2", null ],
    [ "pushToTrajectory", "class_simulation_data_manager.html#ab10b2e7318f9096eeff5add2f5fc4446", null ],
    [ "setDoneFlag", "class_simulation_data_manager.html#ab27a8d08608f72068f2d581119d95d92", null ],
    [ "startNewIteration", "class_simulation_data_manager.html#ac9383e37e39c0bfb7fd533a989a20e6a", null ],
    [ "updateEnginePool", "class_simulation_data_manager.html#a60511fce4b58b63bf71f9b0c62dcb335", null ],
    [ "updateExternalPool", "class_simulation_data_manager.html#a06e0c39af703e1428671d12f28e2821f", null ],
    [ "updatePreprocessingPool", "class_simulation_data_manager.html#a218da465704c7af6b4d599162d88da12", null ],
    [ "updateTransceiverPool", "class_simulation_data_manager.html#a415cab06836a35066303c2665c9bb891", null ]
];