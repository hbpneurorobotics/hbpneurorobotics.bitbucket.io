var computational__node__policies_8h =
[
    [ "ExecutionPolicy", "computational__node__policies_8h.html#a3f872dbefb885b0dca2745e76e002b87", [
      [ "ALWAYS", "computational__node__policies_8h.html#a3f872dbefb885b0dca2745e76e002b87ae7d3c14d40ccda0b6615a55e4d52eeec", null ],
      [ "ON_NEW_INPUT", "computational__node__policies_8h.html#a3f872dbefb885b0dca2745e76e002b87a22a621ec60c2b27f4a0dcdc54d19948e", null ]
    ] ],
    [ "MsgCachePolicy", "computational__node__policies_8h.html#a6e6c639f025a1af2a05b3f20e6b207a5", [
      [ "CLEAR_CACHE", "computational__node__policies_8h.html#a6e6c639f025a1af2a05b3f20e6b207a5ad06cd89641ba0a486411641ef8cf9d15", null ],
      [ "KEEP_CACHE", "computational__node__policies_8h.html#a6e6c639f025a1af2a05b3f20e6b207a5a0fe86060e6b2cc716b117185f3e44ae9", null ]
    ] ],
    [ "MsgPublishPolicy", "computational__node__policies_8h.html#ae65f9d4505207aa68b30fb0419c73035", [
      [ "LAST", "computational__node__policies_8h.html#ae65f9d4505207aa68b30fb0419c73035add704bbffa58fee7b947ac8c26c4e2f4", null ],
      [ "ALL", "computational__node__policies_8h.html#ae65f9d4505207aa68b30fb0419c73035a9dba44949f3309ed09008d2505fd59a9", null ]
    ] ],
    [ "PublishFormatPolicy", "computational__node__policies_8h.html#a92445027731a0eeb5b113134b737119b", [
      [ "SERIES", "computational__node__policies_8h.html#a92445027731a0eeb5b113134b737119ba6ea52d77194fc390647024abd57f78b2", null ],
      [ "BATCH", "computational__node__policies_8h.html#a92445027731a0eeb5b113134b737119ba438aa69f27e0c11c45e587422753a0e4", null ]
    ] ]
];