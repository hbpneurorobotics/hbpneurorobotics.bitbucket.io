var protobuf__ops_8h =
[
    [ "NRPProtobufOpsIface", "classprotobuf__ops_1_1_n_r_p_protobuf_ops_iface.html", "classprotobuf__ops_1_1_n_r_p_protobuf_ops_iface" ],
    [ "NRPProtobufOps", "classprotobuf__ops_1_1_n_r_p_protobuf_ops.html", "classprotobuf__ops_1_1_n_r_p_protobuf_ops" ],
    [ "CREATE_PROTOBUF_OPS", "protobuf__ops_8h.html#aa512c2cc68514357afc7f2f5d89a36b5", null ],
    [ "CREATE_PROTOBUF_OPS_FCN_STR", "protobuf__ops_8h.html#a57ab85957853d0a12f8ad8b88b0cf245", null ],
    [ "getDataPackInterfaceFromMessageSubset", "protobuf__ops_8h.html#a6513736af4b8364ea6a93e33d82e4d3d", null ],
    [ "setDataPackMessageDataSubset", "protobuf__ops_8h.html#ae2a42b44daaf54dcb2e950399c38173c", null ],
    [ "setDataPackMessageFromInterfaceSubset", "protobuf__ops_8h.html#ae862cee267e7331b66a4955d1c3c4d64", null ],
    [ "setTrajectoryMessageFromInterfaceSubset", "protobuf__ops_8h.html#aa92886b42b591183022ef50af1d748a3", null ],
    [ "unpackProtoAnySubset", "protobuf__ops_8h.html#a7db64702e1f5a87d923350d5e37b2dde", null ]
];