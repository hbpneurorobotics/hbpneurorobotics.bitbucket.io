var guides =
[
    [ "General developer guide", "tutorial_developer_guide.html", [
      [ "Testing", "tutorial_developer_guide.html#tutorial_developer_guide_testing", null ],
      [ "Generating and examining core dumps", "tutorial_developer_guide.html#tutorial_developer_guide_core_dumps", null ],
      [ "Logger usage", "tutorial_developer_guide.html#tutorial_developer_guide_loggingg", null ],
      [ "Static code analysis", "tutorial_developer_guide.html#tutorial_developer_guide_static", null ],
      [ "Time Profiler", "tutorial_developer_guide.html#tutorial_developer_guide_time_profiler", null ],
      [ "Debugging gRPC engines", "tutorial_developer_guide.html#tutorial_developer_guide_grpc", null ]
    ] ],
    [ "Creating a new Engine from scratch", "tutorial_engine_creation.html", [
      [ "Directory tree", "tutorial_engine_creation.html#tutorial_engine_creation_directories", null ],
      [ "Setting up CMake", "tutorial_engine_creation.html#tutorial_engine_creation_cmake", null ],
      [ "Creating an Engine configuration schema", "tutorial_engine_creation.html#tutorial_engine_creation_engine_config", [
        [ "Example", "tutorial_engine_creation.html#tutorial_engine_creation_engine_config_example", null ],
        [ "Linking configuration schema to the engine", "tutorial_engine_creation.html#tutorial_engine_creation_engine_config_linking", null ]
      ] ],
      [ "DataPack data", "tutorial_engine_creation.html#tutorial_engine_creation_engine_datapack", null ],
      [ "Creating an EngineClient", "tutorial_engine_creation.html#tutorial_engine_creation_engine_client", [
        [ "Simulation control and state methods", "tutorial_engine_creation.html#tutorial_engine_creation_simulation_control_methods", null ],
        [ "Data exchange methods", "tutorial_engine_creation.html#tutorial_engine_creation_data_exchange_methods", null ],
        [ "Simulation process spawning methods", "tutorial_engine_creation.html#tutorial_engine_creation_simulation_spawning_methods", null ],
        [ "Example", "tutorial_engine_creation.html#tutorial_engine_creation_engine_client_example", null ]
      ] ],
      [ "Creating a Python module", "tutorial_engine_creation.html#tutorial_engine_creation_python", null ],
      [ "Creating a new ProcessLauncher", "tutorial_engine_creation.html#tutorial_engine_creation_engine_proc_launcher", null ],
      [ "Creating an Engine Server", "tutorial_engine_creation.html#tutorial_engine_creation_engine_server", null ]
    ] ],
    [ "Creating a new Engine from template", "engine_creation_template.html", [
      [ "Creating a new engine from template", "engine_creation_template.html#engine_creation_template_script", [
        [ "Engine name", "engine_creation_template.html#engine_creation_template_script_name", null ],
        [ "Generating and compiling code for gRPC engine", "engine_creation_template.html#engine_creation_template_script_grpc", null ],
        [ "Generating and compiling code for JSON engine", "engine_creation_template.html#engine_creation_template_script_json", null ]
      ] ],
      [ "Client side", "engine_creation_template.html#engine_creation_template_client", [
        [ "DataPacks", "engine_creation_template.html#engine_creation_template_datapack", null ],
        [ "Configuration schema", "engine_creation_template.html#engine_creation_template_client_configuration", null ],
        [ "Client class", "engine_creation_template.html#engine_creation_template_client_class", null ]
      ] ],
      [ "Server side", "engine_creation_template.html#engine_creation_template_server", [
        [ "Server class", "engine_creation_template.html#engine_creation_template_server_class", null ],
        [ "DataPack controllers", "engine_creation_template.html#engine_creation_template_server_controllers", null ]
      ] ]
    ] ],
    [ "Compiling new protobuf message definitions", "tutorial_add_proto_definition.html", [
      [ "Using compiled messages in GRPC Engines", "tutorial_add_proto_definition.html#tutorial_using_proto_grpc", null ],
      [ "Using compiled messages in TFs", "tutorial_add_proto_definition.html#tutorial_using_proto_tf", null ],
      [ "Using compiled messages with the Python GRPC Engine", "tutorial_add_proto_definition.html#tutorial_using_proto_python", null ],
      [ "Nested Package Specifiers", "tutorial_add_proto_definition.html#tutorial_add_proto_definition_nested_package_name", null ],
      [ "Complete example", "tutorial_add_proto_definition.html#tutorial_add_proto_definition_example", null ]
    ] ],
    [ "Connecting with ROS from NRP-Core Experiments", "guide_ros_in_nrp_core.html", [
      [ "Generating Python bindings for ROS messages", "guide_ros_in_nrp_core.html#generating_ros_python_bindings", null ],
      [ "API differences between native Python ROS messages and generated Python bindings", "guide_ros_in_nrp_core.html#ros_python_bindings_differences", null ]
    ] ],
    [ "Troubleshooting guide", "guide_troubleshooting.html", [
      [ "Running gazebo experiments on a headless machine", "guide_troubleshooting.html#guide_troubleshooting_gazebo_headless", null ],
      [ "Worker timeout when using Python JSON Engine", "guide_troubleshooting.html#guide_troubleshooting_gunicorn_timeout", null ],
      [ "Symbol Lookup Error in Tests While Building NRP Core", "guide_troubleshooting.html#guide_troubleshooting_test_symbol_lookup_error", null ]
    ] ],
    [ "Building NRPCore Docker images", "docker_compose.html", [
      [ "Variables", "docker_compose.html#docker_compose_variables", null ],
      [ "Parameters", "docker_compose.html#docker_compose_parameters", null ],
      [ "Usage hints", "docker_compose.html#docker_compose_usage", null ]
    ] ],
    [ "Running multiple remote nrp-cores with docker compose", "remote_docker_compose.html", [
      [ "Preparing the environment.", "remote_docker_compose.html#tutorial_compose_initialize", null ]
    ] ],
    [ "Using the Python JSON engine", "python_engine_guide.html", null ],
    [ "Helpful information", "tutorial_helpful_info.html", [
      [ "Additional Models for Braitenberg Husky experiments", "tutorial_helpful_info.html#tutorial_helpful_info_husky", null ],
      [ "Recording progress and generating videos of Gazebo simulation", "tutorial_helpful_info.html#tutorial_helpful_info_gazebo_recording", null ],
      [ "Time Profiling NRPCore Experiments", "tutorial_helpful_info.html#time_profile_tools", null ]
    ] ]
];