var dir_f273d53ada94a0c59b91c9ae303ea8c8 =
[
    [ "input_node.cpp", "nodes_2spinnaker_2input__node_8cpp.html", null ],
    [ "input_node.h", "nodes_2spinnaker_2input__node_8h.html", [
      [ "InputSpinnakerNode", "class_input_spinnaker_node.html", "class_input_spinnaker_node" ],
      [ "InputSpinnakerEdge", "class_input_spinnaker_edge.html", "class_input_spinnaker_edge" ]
    ] ],
    [ "output_node.cpp", "nodes_2spinnaker_2output__node_8cpp.html", null ],
    [ "output_node.h", "nodes_2spinnaker_2output__node_8h.html", [
      [ "OutputSpinnakerNode", "class_output_spinnaker_node.html", "class_output_spinnaker_node" ],
      [ "OutputSpinnakerEdge", "class_output_spinnaker_edge.html", "class_output_spinnaker_edge" ]
    ] ],
    [ "spinnaker_proxy.cpp", "spinnaker__proxy_8cpp.html", null ],
    [ "spinnaker_proxy.h", "spinnaker__proxy_8h.html", "spinnaker__proxy_8h" ]
];