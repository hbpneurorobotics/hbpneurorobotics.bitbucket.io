var class_event_loop =
[
    [ "EventLoop", "class_event_loop.html#a55155b3338a650e2ed6e293fc78d85b3", null ],
    [ "~EventLoop", "class_event_loop.html#acd0f2fd1a93194d07d2c9fe0c855b6a8", null ],
    [ "initializeCB", "class_event_loop.html#ab245e07548c36efd2ddd0d6e6b7facf6", null ],
    [ "runLoopCB", "class_event_loop.html#aaead2822105146e4cc5c4869eb882a39", null ],
    [ "shutdownCB", "class_event_loop.html#a54d8dfe8792a279a1010444f4a2dcc82", null ]
];