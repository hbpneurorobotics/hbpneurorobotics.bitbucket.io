var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwz~",
  1: "bcdefgijlmnoprstwz",
  2: "abcefgijmnopst",
  3: "abcdefghijlmnoprstuwz",
  4: "_abcdefghijlmnoprstuvwz~",
  5: "_abcdefghijlmnopqrstuv",
  6: "acdefgijlmnoprstuv",
  7: "deglmnprs",
  8: "abcefiklnoprsuv",
  9: "cefipst",
  10: "cmnsuv",
  11: "abcdefghilmnprstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Pages"
};

