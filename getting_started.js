var getting_started =
[
    [ "Installation Instructions", "installation.html", [
      [ "Requirements", "installation.html#installation_requirements", [
        [ "Operative System", "installation.html#os", null ],
        [ "NEST", "installation.html#nest_version", null ]
      ] ],
      [ "Dependency Installation", "installation.html#installation_dependencies", null ],
      [ "Installation", "installation.html#installation_procedure", null ],
      [ "Common NRP-core CMake options", "installation.html#installation_options", null ],
      [ "Setting the environment", "installation.html#installation_environment", null ],
      [ "Steps for installing additional simulators", "installation.html#installation_sim", [
        [ "Installation of The Virtual Brain", "installation.html#installation_sim_tvb", null ],
        [ "OpenAI installation", "installation.html#installation_sim_openai", null ],
        [ "Bullet installation", "installation.html#installation_sim_bullet", null ],
        [ "Mujoco installation", "installation.html#installation_sim_mujoco", null ],
        [ "OpenSim installation", "installation.html#installation_sim_opensim", null ]
      ] ]
    ] ],
    [ "Running the example experiments", "running_example_exp.html", [
      [ "Husky braitenberg", "running_example_exp.html#getting_started_experiment_husky", [
        [ "Experiment Variants", "running_example_exp.html#husky_variants", null ]
      ] ],
      [ "DataPack Exchange using the Python JSON Engine", "running_example_exp.html#getting_started_experiment_tf_exchange", null ],
      [ "Pysim experiments", "running_example_exp.html#getting_started_experiment_pysim", [
        [ "Example for OpenAI simulation", "running_example_exp.html#pysim_openai", null ],
        [ "Example for Bullet simulation", "running_example_exp.html#pysim_bullet", null ],
        [ "Example for Mujoco simulation", "running_example_exp.html#pysim_mujoco", null ],
        [ "Example for OpenSim simulation", "running_example_exp.html#pysim_opensim", null ]
      ] ],
      [ "Multi robot experiment", "running_example_exp.html#getting_started_multi_robot", null ]
    ] ],
    [ "Launching Engines in Docker Containers Examples", "running_example_docker_exp.html", null ],
    [ "Launching Engines in Docker Containers Examples with docker-compose", "running_example_docker_compose.html", [
      [ "Launching script", "running_example_docker_compose.html#docker_compose_script", null ],
      [ "Experiment configuration file", "running_example_docker_compose.html#docker_compose_config", null ]
    ] ],
    [ "Running NRP-Core experiments from Python", "python_client.html", [
      [ "Passing data between client and engines", "python_client.html#python_client_status_function", null ],
      [ "Vectorization of NRP Core clients using gym interface", "python_client.html#python_client_vectorization", null ],
      [ "Launching Experiments in Docker Containers", "python_client.html#python_client_docker", null ]
    ] ]
];