var class_pipe_communication =
[
    [ "PipeCommunication", "class_pipe_communication.html#a88910831c937017b5b868e9db8dddd44", null ],
    [ "~PipeCommunication", "class_pipe_communication.html#af7d829ba2da453d1a0d07ab84180f824", null ],
    [ "closeRead", "class_pipe_communication.html#a6648b42e27f9ac10a84583271aa9ee2b", null ],
    [ "closeWrite", "class_pipe_communication.html#a39ef151abd08af515a99a7696e3cdcd0", null ],
    [ "readFd", "class_pipe_communication.html#a519c71b779ddbeb527a6edbf48b63683", null ],
    [ "readP", "class_pipe_communication.html#aa2fcc03de1d67a14cfc5889b5dd863f3", null ],
    [ "writeFd", "class_pipe_communication.html#a73b4aac8219049b52fe27305b583d024", null ],
    [ "writeP", "class_pipe_communication.html#a93c771f5af561fa8dcfceb387f033a53", null ]
];