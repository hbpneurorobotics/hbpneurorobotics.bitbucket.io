var dir_94323606240fbe752c010fbcb471cc4a =
[
    [ "client.py", "client_8py.html", [
      [ "NrpCore", "classclient_1_1_nrp_core.html", "classclient_1_1_nrp_core" ]
    ] ],
    [ "nrp_server_launchers.py", "nrp__server__launchers_8py.html", [
      [ "NRPCoreServerLauncher", "classnrp__server__launchers_1_1_n_r_p_core_server_launcher.html", "classnrp__server__launchers_1_1_n_r_p_core_server_launcher" ],
      [ "NRPCoreForkLauncher", "classnrp__server__launchers_1_1_n_r_p_core_fork_launcher.html", "classnrp__server__launchers_1_1_n_r_p_core_fork_launcher" ],
      [ "NRPCoreDockerLauncher", "classnrp__server__launchers_1_1_n_r_p_core_docker_launcher.html", "classnrp__server__launchers_1_1_n_r_p_core_docker_launcher" ]
    ] ]
];