var classComputationalGraph =
[
    [ "comp_layer", "classComputationalGraph.html#aff8436a039d8210f81e70652632d6bf1", null ],
    [ "GraphState", "classComputationalGraph.html#a6fbec34ede335524562bd9fbe22d52dd", [
      [ "EMPTY", "classComputationalGraph.html#a6fbec34ede335524562bd9fbe22d52dda75cf2ea00a0b1b58c2d83b17715cb625", null ],
      [ "CONFIGURING", "classComputationalGraph.html#a6fbec34ede335524562bd9fbe22d52dda0e5963c0cd8bbb03c53a07d02bb2152a", null ],
      [ "READY", "classComputationalGraph.html#a6fbec34ede335524562bd9fbe22d52dda26bcbd4b609cfc7fd9b44fa2624da9ac", null ],
      [ "COMPUTING", "classComputationalGraph.html#a6fbec34ede335524562bd9fbe22d52dda17bae357217f63706c39e8809dfd7fb3", null ]
    ] ],
    [ "clear", "classComputationalGraph.html#a2bb7bf0f3eac546908c4f9eeb4276e72", null ],
    [ "compute", "classComputationalGraph.html#abebc767f2c382d14790fa9a8debb80a7", null ],
    [ "configure", "classComputationalGraph.html#a55cb7e29b50f9698e34d17b8a66e130b", null ],
    [ "getState", "classComputationalGraph.html#ab4fcb06d1ce7d144d7dabf9e08255d31", null ],
    [ "insert_edge", "classComputationalGraph.html#a67c276b6aba162c2fcecb95e9cde9e59", null ]
];