var classJsonDataPackController =
[
    [ "JsonDataPackController", "classJsonDataPackController.html#ab39b634ff126890af9d964bdba70b68d", null ],
    [ "getCachedData", "classJsonDataPackController.html#a99641aa41674d556b8602ceae35b8ca4", null ],
    [ "getEmptyDataPack", "classJsonDataPackController.html#a23f08f76aa5fc165adc7020ae67f0dbf", null ],
    [ "setCachedData", "classJsonDataPackController.html#a7b6028f336bc4dc4f0ce904d578053eb", null ],
    [ "_data", "classJsonDataPackController.html#a4e2490aa8216de75c317d2abb7a4a52c", null ],
    [ "_datapackId", "classJsonDataPackController.html#a3c76e01b24123550e7b55d6d1eec01f4", null ],
    [ "_emptyDataPack", "classJsonDataPackController.html#a271d354e46bf4028a7821e3f2e32c357", null ]
];