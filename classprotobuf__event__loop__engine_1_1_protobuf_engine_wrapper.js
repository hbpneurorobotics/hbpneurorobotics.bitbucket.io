var classprotobuf__event__loop__engine_1_1_protobuf_engine_wrapper =
[
    [ "__init__", "classprotobuf__event__loop__engine_1_1_protobuf_engine_wrapper.html#ae878577b7a0e6d4fdba8d9ea0edb9b0e", null ],
    [ "get_datapack", "classprotobuf__event__loop__engine_1_1_protobuf_engine_wrapper.html#a8f45698a66b1a8e6abd078d73cc35d3e", null ],
    [ "get_engine_name", "classprotobuf__event__loop__engine_1_1_protobuf_engine_wrapper.html#a64d25acfe4654cce062d64443d18fffa", null ],
    [ "get_registered_datapack_names", "classprotobuf__event__loop__engine_1_1_protobuf_engine_wrapper.html#ab9aa8a12df3d48cac35445a389e4273f", null ],
    [ "initialize", "classprotobuf__event__loop__engine_1_1_protobuf_engine_wrapper.html#a596e94f61e31d2ab97124a3006543444", null ],
    [ "run_loop", "classprotobuf__event__loop__engine_1_1_protobuf_engine_wrapper.html#ac43d023da8de0c928173a4f13ec3e68c", null ],
    [ "set_datapack", "classprotobuf__event__loop__engine_1_1_protobuf_engine_wrapper.html#abd0652ab6b31273ec0f04efb3d39a3a8", null ],
    [ "shutdown", "classprotobuf__event__loop__engine_1_1_protobuf_engine_wrapper.html#ae09585040ee100b755932bf5c4dd1b50", null ]
];