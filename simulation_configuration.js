var simulation_configuration =
[
    [ "General notes about the use of JSON schema", "json_schema.html", [
      [ "Use of JSON Schema in NRP-core", "json_schema.html#json_schema_in_nrp", null ],
      [ "Referencing schemas", "json_schema.html#schema_reference", null ],
      [ "Parameter Default Values", "json_schema.html#default_parameters", null ]
    ] ],
    [ "Simulation Configuration Schema", "simulation_schema.html", [
      [ "Parameters", "simulation_schema.html#simulation_schema_parameters", null ],
      [ "Example", "simulation_schema.html#simulation_schema_example", null ],
      [ "Schema", "simulation_schema.html#simulation_schema_schema", null ]
    ] ],
    [ "Event Loop Schema", "event_loop_schema.html", [
      [ "Parameters", "event_loop_schema.html#event_loop_schema_parameters", null ],
      [ "Example", "event_loop_schema.html#event_loop_schema_example", null ],
      [ "Schema", "event_loop_schema.html#event_loop_schema_schema", null ]
    ] ],
    [ "EngineBase Schema", "engine_base_schema.html", [
      [ "Parameters", "engine_base_schema.html#engine_base_schema_parameters", null ],
      [ "Example", "engine_base_schema.html#engine_base_schema_example", null ],
      [ "Schema", "engine_base_schema.html#engine_base_schema_schema", null ],
      [ "Additional notes on parameters", "engine_base_schema.html#specific_parameters", [
        [ "EngineProcCmd", "engine_base_schema.html#engine_proc_command", null ],
        [ "EngineProcStartParams", "engine_base_schema.html#engine_proc_start_params", null ],
        [ "EngineExtraConfigs", "engine_base_schema.html#engine_extra_configs", null ],
        [ "Additional Configuration Parameters", "engine_base_schema.html#additional_engine_configuration", null ]
      ] ]
    ] ],
    [ "Transceiver Function Schema", "transceiver_function_schema.html", [
      [ "Parameters", "transceiver_function_schema.html#transceiver_function_schema_parameters", null ],
      [ "Example", "transceiver_function_schema.html#transceiver_function_schema_example", null ],
      [ "Schema", "transceiver_function_schema.html#transceiver_function_schema_schema", null ]
    ] ],
    [ "Process Launcher Schema", "process_launcher_schema.html", [
      [ "Parameters", "process_launcher_schema.html#process_launcher_schema_parameters", null ],
      [ "Example", "process_launcher_schema.html#process_launcher_schema_example", null ],
      [ "Schema", "process_launcher_schema.html#process_launcher_schema_schema", null ]
    ] ],
    [ "NRP Connectors Schema", "nrp_connectors_schema.html", [
      [ "ROSNode Parameters", "nrp_connectors_schema.html#ros_connector_schema_parameters", null ],
      [ "MQTTClient Parameters", "nrp_connectors_schema.html#mqtt_connector_schema_parameters", null ],
      [ "Example", "nrp_connectors_schema.html#nrp_connectors_schema_example", null ],
      [ "Schema", "nrp_connectors_schema.html#nrp_connectors_schema_schema", null ]
    ] ]
];