var nrp_engines =
[
    [ "Engine Communication Protocols", "engine_comm.html", [
      [ "JSON over REST", "engine_comm.html#engine_json", [
        [ "Engine Configuration Parameters", "engine_comm.html#engine_json_config_section", null ]
      ] ],
      [ "Protobuf over gRPC", "engine_comm.html#engine_grpc", [
        [ "Protobuf libraries in NRPCore", "engine_comm.html#engine_grpc_protobuf_libraries", null ],
        [ "Engine Configuration Parameters", "engine_comm.html#engine_grpc_config_section", null ]
      ] ],
      [ "Schema", "engine_comm.html#engine_comm_protocols_schema", null ]
    ] ],
    [ "Gazebo Engine", "gazebo_engine.html", "gazebo_engine" ],
    [ "NEST Engine", "nest_engine.html", "nest_engine" ],
    [ "Python JSON Engine", "python_json_engine.html", [
      [ "EngineScript", "python_json_engine.html#python_json_engine_script", null ],
      [ "DataPacks", "python_json_engine.html#python_json_datapacks", null ],
      [ "Engine Configuration Parameters", "python_json_engine.html#python_json_configuration", null ],
      [ "Schema", "python_json_engine.html#python_json_schema", null ]
    ] ],
    [ "Python GRPC Engine", "python_grpc_engine.html", [
      [ "GrpcEngineScript", "python_grpc_engine.html#python_grpc_engine_script", null ],
      [ "DataPacks", "python_grpc_engine.html#python_grpc_datapacks", null ],
      [ "Engine Configuration Parameters", "python_grpc_engine.html#python_grpc_configuration", null ],
      [ "Schema", "python_grpc_engine.html#python_grpc_schema", null ]
    ] ],
    [ "PySim Engine", "pysim_engine.html", [
      [ "SimulatorManager", "pysim_engine.html#simulator_manager", null ],
      [ "DataPacks", "pysim_engine.html#opensim_json_datapacks", null ],
      [ "Engine Configuration Parameters", "pysim_engine.html#engine_opensim_config_section", null ],
      [ "Schema", "pysim_engine.html#engine_opensim_schema", null ]
    ] ],
    [ "The Virtual Brain Engine", "tvb_engine.html", [
      [ "Defining proxy nodes in the brain model", "tvb_engine.html#tvb_engine_proxy_nodes", null ],
      [ "Extending the Simulator class", "tvb_engine.html#tvb_engine_cosim", null ],
      [ "Injecting data into the proxy region", "tvb_engine.html#tvb_engine_injecting_data", null ]
    ] ],
    [ "DataTransfer Engine", "datatransfer_engine.html", [
      [ "Enabling Data Streaming with MQTT", "datatransfer_engine.html#datatransfer_engine_streaming", null ],
      [ "DataPacks", "datatransfer_engine.html#datatransfer_engine_datapacks", [
        [ "Dump Protobuf message types", "datatransfer_engine.html#dump_proto", [
          [ "Logging arrays with Dump.ArrayFloat", "datatransfer_engine.html#dump_array_float", null ]
        ] ],
        [ "Logging and streaming address", "datatransfer_engine.html#logging_address", null ]
      ] ],
      [ "Engine Configuration Parameters", "datatransfer_engine.html#engine_datatransfer_config_section", null ],
      [ "Schema", "datatransfer_engine.html#engine_datatransfer_schema", null ]
    ] ],
    [ "EDLUT Engine", "edlut_engine.html", [
      [ "EDLUT Datapacks", "edlut_engine.html#edlut_engine_datapacks", null ],
      [ "Engine Configuration Parameters", "edlut_engine.html#engine_edlut_config_section", null ],
      [ "Schema", "edlut_engine.html#engine_edlut_schema", null ]
    ] ]
];