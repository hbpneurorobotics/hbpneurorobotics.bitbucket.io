var architecture_overview =
[
    [ "Synchronization Model", "sync_model_details.html", [
      [ "Simulation time and time frame", "sync_model_details.html#sim_timeline", null ],
      [ "Communication with Engines", "sync_model_details.html#engine_sync", null ],
      [ "Simulation Loop step structure", "sync_model_details.html#step_structure", null ],
      [ "case t=0, first simulation loop and empty datapacks", "sync_model_details.html#initial_loop", null ]
    ] ],
    [ "Event Loop", "event_loop.html", "event_loop" ],
    [ "Engine Plugin System", "plugin_system.html", null ],
    [ "Process Launcher", "process_launcher.html", null ],
    [ "Launch Commands", "launch_command.html", [
      [ "Configuration Schema", "launch_command.html#configuration_schema", null ],
      [ "DockerLauncher", "launch_command.html#docker_launcher", [
        [ "Remote Engine configuration", "launch_command.html#docker_launcher_engine_param", null ],
        [ "Schema", "launch_command.html#docker_launcher_schema", null ],
        [ "Usage", "launch_command.html#docker_launcher_engines", null ]
      ] ],
      [ "Launching experiments with docker-compose", "launch_command.html#docker_compose_launch", null ],
      [ "docker-compose file", "launch_command.html#docker_compose_file", null ]
    ] ],
    [ "Experiment Lifecycle", "experiment_lifecycle.html", "experiment_lifecycle" ],
    [ "NRPCoreSim", "nrp_simulation.html", [
      [ "CLI for overriding configuration file", "nrp_simulation.html#cli_override_simulation_parameters", null ],
      [ "Server Mode", "nrp_simulation.html#server_mode", null ]
    ] ],
    [ "Main Elements", "main_elements.html", "main_elements" ],
    [ "Supported Workflows in NRP Core", "supported_workflows.html", [
      [ "Standalone Simulation", "supported_workflows.html#supported_workflows_basic", null ],
      [ "Simulation driven by a Master Script", "supported_workflows.html#supported_workflows_client", null ]
    ] ]
];