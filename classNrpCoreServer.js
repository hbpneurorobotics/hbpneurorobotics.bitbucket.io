var classNrpCoreServer =
[
    [ "RequestType", "classNrpCoreServer.html#a5c39bb778ee26f83e26b2fb6a5d9794b", [
      [ "None", "classNrpCoreServer.html#a5c39bb778ee26f83e26b2fb6a5d9794ba6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "Init", "classNrpCoreServer.html#a5c39bb778ee26f83e26b2fb6a5d9794ba95b19f7739b0b7ea7d6b07586be54f36", null ],
      [ "RunLoop", "classNrpCoreServer.html#a5c39bb778ee26f83e26b2fb6a5d9794ba044bf8df59efae7e4bda6b803c11bb36", null ],
      [ "Shutdown", "classNrpCoreServer.html#a5c39bb778ee26f83e26b2fb6a5d9794ba1a4ebb180ba59b067782515ffee6e975", null ]
    ] ],
    [ "NrpCoreServer", "classNrpCoreServer.html#a370f24272a9d1c1c5bd491757d4353bc", null ],
    [ "getNumIterations", "classNrpCoreServer.html#a969e6f33637866aeee3619ed4a75141c", null ],
    [ "getRequestType", "classNrpCoreServer.html#a15853f1da23de05ed97df189db8c9b6e", null ],
    [ "markRequestAsFailed", "classNrpCoreServer.html#addbe4268e67f934b6ce1fc313f2d2352", null ],
    [ "markRequestAsProcessed", "classNrpCoreServer.html#a71b3318e52960f9b07414c22a5d17001", null ],
    [ "waitForRequest", "classNrpCoreServer.html#af5d22eb7b393f335ba1ea3026389c0e5", null ]
];