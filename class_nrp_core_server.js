var class_nrp_core_server =
[
    [ "RequestType", "class_nrp_core_server.html#a5c39bb778ee26f83e26b2fb6a5d9794b", [
      [ "None", "class_nrp_core_server.html#a5c39bb778ee26f83e26b2fb6a5d9794ba6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "Initialize", "class_nrp_core_server.html#a5c39bb778ee26f83e26b2fb6a5d9794ba223e06f5cb15fc701ea1e686f7934f4f", null ],
      [ "RunLoop", "class_nrp_core_server.html#a5c39bb778ee26f83e26b2fb6a5d9794ba044bf8df59efae7e4bda6b803c11bb36", null ],
      [ "RunUntilTimeout", "class_nrp_core_server.html#a5c39bb778ee26f83e26b2fb6a5d9794ba4448ce5bc16ff8588056976b94d3b415", null ],
      [ "StopLoop", "class_nrp_core_server.html#a5c39bb778ee26f83e26b2fb6a5d9794ba27f65bfa9826e6296dac07b29bfe6727", null ],
      [ "Reset", "class_nrp_core_server.html#a5c39bb778ee26f83e26b2fb6a5d9794ba526d688f37a86d3c3f27d0c5016eb71d", null ],
      [ "Shutdown", "class_nrp_core_server.html#a5c39bb778ee26f83e26b2fb6a5d9794ba1a4ebb180ba59b067782515ffee6e975", null ]
    ] ],
    [ "NrpCoreServer", "class_nrp_core_server.html#af8e387ff7924efbf98379c510622b5db", null ],
    [ "runServerLoop", "class_nrp_core_server.html#a2b5b0efe31ea66c56c429051b6669ab0", null ],
    [ "stopServerLoop", "class_nrp_core_server.html#a1c8bfc65f087a21d5feecc2dd13f4d0f", null ]
];