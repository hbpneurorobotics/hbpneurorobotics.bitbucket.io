var tutorial_engine_creation =
[
    [ "Directory tree", "tutorial_engine_creation.html#tutorial_engine_creation_directories", null ],
    [ "Setting up CMake", "tutorial_engine_creation.html#tutorial_engine_creation_cmake", null ],
    [ "Creating an Engine configuration schema", "tutorial_engine_creation.html#tutorial_engine_creation_engine_config", [
      [ "Example", "tutorial_engine_creation.html#tutorial_engine_creation_engine_config_example", null ],
      [ "Linking configuration schema to the engine", "tutorial_engine_creation.html#tutorial_engine_creation_engine_config_linking", null ]
    ] ],
    [ "DataPack data", "tutorial_engine_creation.html#tutorial_engine_creation_engine_datapack", null ],
    [ "Creating an EngineClient", "tutorial_engine_creation.html#tutorial_engine_creation_engine_client", [
      [ "Simulation control and state methods", "tutorial_engine_creation.html#tutorial_engine_creation_simulation_control_methods", null ],
      [ "Data exchange methods", "tutorial_engine_creation.html#tutorial_engine_creation_data_exchange_methods", null ],
      [ "Simulation process spawning methods", "tutorial_engine_creation.html#tutorial_engine_creation_simulation_spawning_methods", null ],
      [ "Example", "tutorial_engine_creation.html#tutorial_engine_creation_engine_client_example", null ]
    ] ],
    [ "Creating a Python module", "tutorial_engine_creation.html#tutorial_engine_creation_python", null ],
    [ "Creating a new ProcessLauncher", "tutorial_engine_creation.html#tutorial_engine_creation_engine_proc_launcher", null ],
    [ "Creating an Engine Server", "tutorial_engine_creation.html#tutorial_engine_creation_engine_server", null ],
    [ "Explanation of Engine CMakeLists.txt", "tutorial_engine_creation_engine_cmake_example_explanation.html", null ]
];