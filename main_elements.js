var main_elements =
[
    [ "Engine DataPacks", "datapacks.html", [
      [ "DataPack ID", "datapacks.html#datapacks_id", null ],
      [ "DataPack data", "datapacks.html#datapacks_data", null ],
      [ "Empty DataPacks", "datapacks.html#empty_datapack", null ],
      [ "Checking if a DataPack contains the most recent data", "datapacks.html#stale_datapacks", null ],
      [ "Role of DataPacks in TransceiverFunctions", "datapacks.html#datapacks_tfs", [
        [ "DataPacks as input to transceiver functions", "datapacks.html#datapacks_tfs_input", null ],
        [ "DataPacks as output of transceiver functions", "datapacks.html#datapacks_tfs_output", null ]
      ] ],
      [ "Supported DataPack data types", "datapacks.html#supported_datapack_types", [
        [ "JsonDataPack", "datapacks.html#datapacks_json", [
          [ "Importing and creating JsonDataPack", "datapacks.html#datapacks_json_importing", null ],
          [ "Getting and setting data", "datapacks.html#datapacks_json_setting_getting", null ],
          [ "Inspecting content of JsonDataPack", "datapacks.html#datapacks_json_inspecting", null ],
          [ "Using JsonDataPacks to store JSON arrays", "datapacks.html#datapacks_json_arrays", null ]
        ] ],
        [ "Protobuf DataPacks", "datapacks.html#datapacks_protobuf", [
          [ "Generic protobuf DataPacks and wrappers for well-known protobuf types", "datapacks.html#datapacks_protobuf_generic", null ]
        ] ],
        [ "ROS msg datapacks", "datapacks.html#datapacks_rosmsg", null ]
      ] ],
      [ "Implementation details", "datapacks.html#datapacks_implementation", [
        [ "Empty datapacks", "datapacks.html#datapacks_implementation_empty", null ],
        [ "Python interface", "datapacks.html#datapacks_implementation_python", null ]
      ] ]
    ] ],
    [ "Functions", "nrp_functions.html", [
      [ "Simulation time and iteration number", "nrp_functions.html#functions_time_iteration", null ]
    ] ],
    [ "Transceiver Functions", "transceiver_function.html", [
      [ "Example", "transceiver_function.html#transceiver_function_example", null ],
      [ "Synchronization", "transceiver_function.html#transceiver_function_synchronization", null ],
      [ "Implementation Details", "transceiver_function.html#transceiver_function_implementation", null ]
    ] ],
    [ "Preprocessing Functions", "preprocessing_function.html", null ],
    [ "Status Functions", "status_function.html", [
      [ "Status Function definition", "status_function.html#status_function_definition", null ],
      [ "Input arguments", "status_function.html#status_function_input", null ],
      [ "Return values", "status_function.html#status_function_return", null ],
      [ "Examples", "status_function.html#status_function_example", null ],
      [ "Configuration", "status_function.html#status_function_configuration", null ]
    ] ],
    [ "Engines", "engines.html", [
      [ "Python JSON Engine", "engines.html#Python_json_engine_section", null ],
      [ "Engine Launchers", "engines.html#engine_launchers", null ]
    ] ],
    [ "Simulation Loop", "simulation_loop.html", null ]
];