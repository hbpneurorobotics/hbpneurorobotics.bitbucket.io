var class_n_r_p_spinnaker_proxy =
[
    [ "~NRPSpinnakerProxy", "class_n_r_p_spinnaker_proxy.html#a30dc9bcc397933d9849dc7b0fa650e6f", null ],
    [ "NRPSpinnakerProxy", "class_n_r_p_spinnaker_proxy.html#a0a20e7474f343ce5f370d2a6d7bd5f84", null ],
    [ "NRPSpinnakerProxy", "class_n_r_p_spinnaker_proxy.html#a87e62ca3c5bb7ed13ed510e92b1dee8d", null ],
    [ "addReceiver", "class_n_r_p_spinnaker_proxy.html#ade7df3e39976fc9727e7495690a0616c", null ],
    [ "addSender", "class_n_r_p_spinnaker_proxy.html#a4656f85a94acfa0cdd7eb3c420908a28", null ],
    [ "operator=", "class_n_r_p_spinnaker_proxy.html#a2ebdff85a1c1ff7687ce1716c84ad10e", null ],
    [ "operator=", "class_n_r_p_spinnaker_proxy.html#af1a92960462a748a4c18d52f95c14ecf", null ],
    [ "receive_payloads", "class_n_r_p_spinnaker_proxy.html#adaa74663f6246f2b32eedeba33740d44", null ],
    [ "receive_spikes", "class_n_r_p_spinnaker_proxy.html#a4ec585f8e5e51b27f2cf674948ff583f", null ],
    [ "runSpinnaker", "class_n_r_p_spinnaker_proxy.html#a32ad91163b6c25469e0d64cfd41940a2", null ],
    [ "send", "class_n_r_p_spinnaker_proxy.html#a310e7699cd92339f8e47b983b7ddc33f", null ],
    [ "spikes_start", "class_n_r_p_spinnaker_proxy.html#adfed31f8001f6f62ca5153bc2ce25a8e", null ],
    [ "startSpinnaker", "class_n_r_p_spinnaker_proxy.html#a91ae90e2c2a07bfdc88e113b21e94d81", null ],
    [ "stopSpinnaker", "class_n_r_p_spinnaker_proxy.html#a9f46bc37fa7b1d1d522d601a13a0c768", null ]
];